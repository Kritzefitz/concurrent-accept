{-# LANGUAGE RankNTypes #-}
{-|
Description: Serve incoming connections on a set of sockets
 concurrently
Copyright  : 2018 Sven Bartscher
License    : MPL-2.0
Maintainer : Sven Bartscher <sven.bartscher@weltraumschlangen.de>
Stability  : experimental

This module allows starting servers that listen on multiple sockets and
handle incoming TCP connections concurrently with proper resource cleanup.
-}

module Network.Concurrent
    (
    -- * Starting servers
    -- $starting
    serveBlocking
    , serveNonblocking
    -- * Specifying server behaviour
    -- $behaviour
    , ServerSpec(..)
    , ListenSpec(..)
    , HandlerCleanup(..)
    , killHandlers
    , timeoutHandlers
    , drainHandlers
    , ConnHandler
    , simpleMultiServer
    -- * Stopping
    -- $stopping

    -- ** Internally
    , Stopper
    -- ** Blocking
    -- $blockingStopping

    -- ** Nonblocking
    -- $nonblockingStopping
    , ServerHandle
    , waitServer
    , waitServerCatch
    , terminateServer
    , terminateServerAndWait
    , terminateServerAndWaitCatch
    , cancelServer
    , pollServer
    -- * Exception handling
    -- $exceptionHandling
    , RecoverListener(..)
    -- *Re-Exports
    , Handle
    , Handler(..)
    , HostName
    , PortNumber
    ) where

import Control.Concurrent.Async ( Async
                                , AsyncCancelled(AsyncCancelled)
                                , async
                                , asyncWithUnmask
                                , cancel
                                , poll
                                , wait
                                , waitCatch
                                , withAsyncWithUnmask
                                )
import Control.Concurrent.MVar ( MVar
                               , newEmptyMVar
                               , takeMVar
                               , tryPutMVar
                               )
import Control.Exception ( AsyncException(ThreadKilled)
                         , Exception
                         , Handler(Handler)
                         , SomeException
                         , bracket
                         , catches
                         , finally
                         , fromException
                         )
import Control.Monad ( forever
                     , void
                     , when
                     )
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Resource ( ResIO
                                    , ReleaseKey
                                    , allocate
                                    , release
                                    , resourceMask
                                    , runResourceT
                                    , throwM
                                    )
import Data.IORef ( IORef
                  , newIORef
                  , modifyIORef
                  , readIORef
                  , writeIORef
                  )
import Network.Socket ( Family
                      , HostName
                      , PortNumber
                      , ProtocolNumber
                      , SockAddr(SockAddrUnix)
                      , Socket
                      , SocketOption
                      , SocketType
                      , accept
                      , bind
                      , close
                      , listen
                      , maxListenQueue
                      , setSocketOption
                      , socket
                      )
import System.Directory (removeFile)
import System.IO ( Handle
                 )
import System.IO.Error ( catchIOError
                       , isDoesNotExistError
                       )
import System.Timeout (timeout)

-- $starting
-- Servers can be started either in blocking or nonblocking mode. See
-- below for a description of what each of those means.

-- |Starts a server according to the given 'ServerSpec's and blocks
-- until the server is stopped by a call to the 'Stopper' supplied to
-- the 'ConnHandler's. This might throw exceptions as described in
-- [Exception handling](#exceptionHandling)
serveBlocking :: [ServerSpec]              -- ^Defines the behaviour
                                           -- of the server
              -> IO ()
serveBlocking listenSpecs = do
  stopper <- newEmptyMVar
  serve listenSpecs stopper

-- |Starts a server asynchronously and returns a 'ServerHandle'
-- immediately. This has the effect that the executing thread isn't
-- blocked until the server terminates as it is the case with
-- 'serveBlocking'. The started server can later be stopped as
-- [described below](#nonblockingStopping).
serveNonblocking :: [ServerSpec] -> IO ServerHandle
serveNonblocking listenSpecs = do
  stopper <- newEmptyMVar
  thread <- async $ serve listenSpecs stopper
  return $ ServerHandle { serverStopper = () <$ tryPutMVar stopper ()
                        , serverThread = thread
                        }

-- |This call actually starts serving. It blocks, but can be told to
-- terminate via putting @()@ into the passed 'MVar'.
serve :: [ServerSpec] -> MVar () -> IO ()
serve serverSpecs stopper = do
  let stop = void $ tryPutMVar stopper ()
  withAll' withAsyncWithUnmask
               (map (\serverSpec unmask ->
                         unmask $ listener stop serverSpec
                    ) serverSpecs)
               $ \listenerThreads -> withAsyncWithUnmask ($ monitor stop listenerThreads)
                                     $ \monitoringThread -> takeMVar stopper
                                                            *> mapM_ cancel listenerThreads
                                                            *> wait monitoringThread

-- |Wait for all given 'Async's and stop when all of them terminated
-- or one of them throws an exception.
monitor :: Stopper -> [Async a] -> IO ()
monitor stop threads = mapM_ waitIgnoreThreadKilled threads `finally` stop
    where waitIgnoreThreadKilled thread = waitCatch thread
                                          >>= either ignoreThreadKilled (const (return ()))
          ignoreThreadKilled e = if isThreadKilledException e || isAsyncCancelledException e
                                 then pure ()
                                 else throwM e
          isThreadKilledException e = fromException e == Just ThreadKilled
          isAsyncCancelledException e = fromException e == Just AsyncCancelled

-- |Serve one socket according to the given 'ServerSpec'
listener :: Stopper -> ServerSpec -> IO ()
listener stopper spec@(ServerSpec handler listenSpec handleLeftovers errorHandlers) =
    (bracket (obtainSocket listenSpec) close $ \listenSocket ->
         runResourceT $ acceptLoop (supplyStopper handler stopper) handleLeftovers listenSocket
    ) `catches` map (mapHandler ((>>= recoverListener stopper spec) .)) errorHandlers

-- |Obtain a 'Socket' from the given 'ListenSpec'
obtainSocket :: ListenSpec -> IO Socket
obtainSocket (NewSocket socketType family address protocolNumber sockOpts) = do
  case address of
    SockAddrUnix p -> when (head p /= '\00') $
                      catchIOError (removeFile p) $ \e -> if isDoesNotExistError e
                                                          then return ()
                                                          else ioError e
    _ -> return ()
  s <- socket family socketType protocolNumber
  mapM_ (uncurry $ setSocketOption s) sockOpts
  bind s address
  listen s maxListenQueue
  pure s
obtainSocket (ExistingSocket s) = return s

-- |Accept on the socket in a loop. In between, make sure that
-- references to dead threads are cleaned up.
acceptLoop :: StoppingConnHandler
           -> HandlerCleanup
           -> Socket
           -> ResIO ()
acceptLoop handler handleLeftovers listenSocket = do
  runningThreads <- liftIO $ newIORef []
  forever $ do
    acceptAndStartServer runningThreads handler handleLeftovers listenSocket
    cleanupThreads runningThreads

-- |Accept a connection on the given 'Socket' and start the given
-- handler with the accepted connection. Makes sure that the started
-- threads are cleaned up according to the given
-- 'HandleLeftoverHandlers'. Started threads are registered in the
-- 'IORef'.
acceptAndStartServer :: IORef [(ReleaseKey, Async ())]
                     -> StoppingConnHandler
                     -> HandlerCleanup
                     -> Socket
                     -> ResIO ()
acceptAndStartServer startedThreads handler handleLeftovers listenSocket =
    resourceMask $ \restore -> do
      (connSocket, peer) <- restore $ liftIO $ accept listenSocket
      thread <- resourceAsync handleLeftovers
                $ asyncWithUnmask ($ (handler connSocket peer
                                       `finally` close connSocket))
      liftIO $ modifyIORef startedThreads (thread:)

-- |Run the given action and ensure that the resulting 'Async' is
-- cleand up according to the given 'HandleLeftoverHandlers'
resourceAsync :: HandlerCleanup
              -> IO (Async a)
              -> ResIO (ReleaseKey, Async a)
resourceAsync handleLeftover asyncF =
    allocate asyncF $ stopHandler handleLeftover

-- |Search the given 'IORef' for stopped thread and remove them from
-- the 'IORef' and relese them from the 'ResIO'
cleanupThreads :: IORef [(ReleaseKey, Async ())] -> ResIO ()
cleanupThreads threadsRef = do
  threads <- liftIO $ readIORef threadsRef
  (stillRunningThreads, stoppedThreads) <- partitionA (checkThreadRunning . snd) threads
  liftIO $ writeIORef threadsRef stillRunningThreads
  mapM_ (release . fst) stoppedThreads
      where checkThreadRunning thread = liftIO (poll thread) >>=
                                        maybe (return True)
                                                  (either throwM $ const $ return False)

-- $behaviour
-- A server can serve connections on multiple sockets, each with their
-- own behaviour in regards to what socket to listen on, how to handle
-- incoming connections on that socket and what to do with
-- 'ConnHandlers' that are still running when the server
-- terminates. This behaviour is passed as a list of 'ServerSpec's.

-- |This structure specifies how a server should serve a given
-- socket.
data ServerSpec = ServerSpec { -- |How to handle incoming connections
                               serverHandler :: ConnHandler,
                               -- |Where to listen
                               serverAddress :: ListenSpec,
                               -- |What to do with ConnHandlers that
                               -- are still running when the server
                               -- terminates
                               serverLeftoverHandling :: HandlerCleanup,
                               -- |The error handlers installed in the
                               -- listener for this socket.
                               serverErrorHandlers :: [Handler RecoverListener]
    }

-- |Specifies a socket a server should listen on.
data ListenSpec
    -- |Open a new socket on the given address. If a 'UnixSocket' is
    -- given and a file already exists under the given path, it will
    -- be unlinked.
    = NewSocket !SocketType !Family !SockAddr !ProtocolNumber [(SocketOption, Int)]
    -- |Specifies a socket that was obtained in some other way and is
    -- ready to 'accept' connections.
    | ExistingSocket !Socket

-- |Specifies what the server should do with 'ConnHandlers' that are
-- still running when the server terminates (either normally or
-- because of an exception).
--
-- You can specify custom cleanup of handler threads by supplying your
-- own function to the constructor.
newtype HandlerCleanup = HandlerCleanup {stopHandler :: forall a. Async a -> IO ()}

-- |Attempt to kill all running 'ConnHandlers' immediately by
-- throwing them a 'ThreadKilled' exception. “immediately” is only
-- as immediate as Haskell's asynchronous exceptions allows and
-- might be a quite long time if the handlers have 'mask'ed
-- asynchronous exceptions.
killHandlers :: HandlerCleanup
killHandlers = HandlerCleanup cancel

-- |Wait up to the supplied number of microseconds for the
-- 'ConnHandler' to terminate by itself (it will not be notified
-- that it should terminate). If the specified time runs out and
-- the 'ConnHandler' still didn't terminate, proceed with the given
-- HandlerCleanup.
--
-- This is currently buggy, as the timeouts run sequentially for
-- each 'ConnHandler'.
timeoutHandlers :: Int -> HandlerCleanup -> HandlerCleanup
timeoutHandlers t (HandlerCleanup kill) =
    HandlerCleanup $ \thread -> timeout t (wait thread) >>= maybe (kill thread) (const $ pure ())

-- |Wait for the 'ConnHandler' to terminate by itself (it will not
-- be notified that it should terminate). This will wait forever
-- if the 'ConnHandler' runs forever.
drainHandlers :: HandlerCleanup
drainHandlers = HandlerCleanup $ (()<$) . wait

-- |A function to handle an incoming connection. The arguments it
-- receives are in order: the 'Handle' to communicate with the client,
-- the 'HostName' of the peer socket, the 'PortNumber' of the remote
-- connection, and a 'Stopper' to stop the running server.
type ConnHandler = Socket -> SockAddr -> Stopper -> IO ()
-- |A variant of ConnHandler that doesn't get a Stopper. Presumably
-- the Stopper was already supplied and is now inside the closure.
type StoppingConnHandler = Socket -> SockAddr -> IO ()

-- |This is a helper function for the case when you want to start a
-- server that listens on multiple sockets but has exactly the same
-- behavior for all sockets.
simpleMultiServer :: ConnHandler
                  -> HandlerCleanup
                  -> [Handler RecoverListener]
                  -> [ListenSpec]
                  -> [ServerSpec]
simpleMultiServer handler handleLeftovers errorHandlers =
    map $ \listenSpec -> ServerSpec { serverHandler = handler
                                    , serverLeftoverHandling = handleLeftovers
                                    , serverAddress = listenSpec
                                    , serverErrorHandlers = errorHandlers
                                    }

-- $stopping
-- Servers can be orderly stopped through various means described
-- below. They can also be stopped by uncaught exceptions, in which
-- case they will terminate as described in [Exception
-- handling](#exceptionHandling)

-- |In a running server each 'ConnHandler' and exception 'Handler' will
-- be passed a 'Stopper' which, when called, will signal the server to
-- stop all listeners and 'ConnHandlers' according to the specified
-- 'HandleLeftoverHandlers'.
--
-- It is safe to pass a 'Stopper' outside of the thread it was given
-- to and call it from there. If you are doing this to stop the server
-- from the outside, using the 'ServerHandle' returned by
-- 'serveNonblocking' might be more appropriate though.
type Stopper = IO ()

-- |Supply a Stopper to a ConnHandler
supplyStopper :: ConnHandler -> Stopper -> StoppingConnHandler
supplyStopper handler stopper handle peer = handler handle peer stopper

-- $blockingStopping
-- A server that was started in blocking mode can only be orderly
-- terminated by a call to a 'Stopper'.

-- $nonblockingStopping
-- #nonblockingStopping#
-- A server that was started in nonblocking mode can be stopped from
-- the outside with the functions below.

-- |This structure is used to refer to a asynchronously started
-- server.
data ServerHandle = ServerHandle {
    -- |The action to stop the server gracefully
    serverStopper :: IO (),
    -- |The control thread of the server
    serverThread :: Async ()
    }

-- |Blocks until the server terminates by itself (it will not be
-- signaled that it should terminate). If the server terminates
-- because of an unhandled exception it will be re-thrown.
waitServer :: ServerHandle -> IO ()
waitServer = wait . serverThread

-- |Like 'waitServer', but if an unhandled exception occured it will
-- be returned instead of re-thrown.
waitServerCatch :: ServerHandle -> IO (Maybe SomeException)
waitServerCatch = fmap (either Just $ const Nothing) . waitCatch . serverThread

-- |Signals the server that it should orderly terminate, but does not
-- wait until the server actually terminated.
terminateServer :: ServerHandle -> IO ()
terminateServer = serverStopper

-- |'terminateServer' followed by 'wait'
terminateServerAndWait :: ServerHandle -> IO ()
terminateServerAndWait server = terminateServer server *> waitServer server

-- |'terminateServer' followed by 'waitCatch'
terminateServerAndWaitCatch :: ServerHandle -> IO (Maybe SomeException)
terminateServerAndWaitCatch server = terminateServer server *> waitServerCatch server

-- |Attempts to kill the server by throwing a 'ThreadKilled' exception
-- to it and waits until the server actually terminates. This should
-- rarely be necessary as 'terminateServer' is usually a better
-- option.
cancelServer :: ServerHandle -> IO ()
cancelServer = cancel . serverThread

-- |Checks if the server is still running. If it is, this returns
-- 'Nothing', otherwise this returns @'Just' 'Nothing'@ if the server
-- exited orderly or @'Just' $ 'Just' e@ if the server exited because
-- of the unhandled exception @e@
pollServer :: ServerHandle -> IO (Maybe (Maybe SomeException))
pollServer = fmap (fmap $ either Just (const Nothing)) . poll . serverThread

-- $exceptionHandling
-- #exceptionHandling#
-- During a servers lifetime exceptions can be thrown in lots of
-- places. This ranges from predictable exceptions like failing to
-- open one of the specified socket to more unexpected exceptions like
-- exceptions that were not handled in a 'ConnHandler' or asynchronous
-- exceptions anytime during listening on the socket.
--
-- There are various places where the caller of the server can
-- influence the handling of exceptions:
--
-- 1. You can catch exceptions in 'ConnHandler's inside of them. If an
--    exception is not handled inside the 'ConnHandler' and thus
--    aborts it, it will propagate to the listener. This is not
--    guaranteed to (and usually won't) happen immediately, as the
--    listener thread usually has better things to do, than checking
--    if one of the 'ConnHandlers' threw an exception.
--
-- 2. You can specify a list of error handlers as part of a
--    'ServerSpec'. They get the chance to handle exceptions that were
--    thrown while opening the socket or listening on the socket. If
--    exceptions propagated from a 'ConnHandler' they can also be
--    handled here. The 'Handler' that handled an exception can alse
--    decide how the server should continue running after the
--    exception was thrown.
--
-- Exceptions that don't get caught in the listener propagate to the
-- top of the server and cause the whole server to terminate.

-- |Specifies how the server should continue running after an
-- exception was handled
data RecoverListener
    -- |All is lost. The whole server should terminate.
    = StopServer
    -- |Not all is lost, but this listener shouldn't run anymore.
    | StopListener
    -- |The listener should be restarted. Beware that blindly
    -- restarting the listener might just throw the same exception
    -- again and thus result in an endless loop!
    | RestartListener

-- |Execute the action selected by an exception 'Handler'
recoverListener :: Stopper -> ServerSpec -> RecoverListener -> IO ()
recoverListener stop _ StopServer = stop
recoverListener _ _ StopListener = return ()
recoverListener stop spec RestartListener = listener stop spec

-- |modify a 'Handler' by applying the given function to the handler
-- function
mapHandler :: (forall e. Exception e => (e -> IO a) -> e -> IO b)
           -> Handler a
           -> Handler b
mapHandler modify (Handler handle) = Handler $ modify handle

----- Utilities -----

-- |Like 'partition' but allows the function to return an
-- 'Applicative'
partitionA :: (Applicative f) => (a -> f Bool) -> [a] -> f ([a], [a])
partitionA f = foldr (\x -> ((\r -> if r then toFst x else toSnd x) <$> f x <*>))
               (pure ([], []))
    where toFst x (fsts, snds) = (x:fsts, snds)
          toSnd x (fsts, snds) = (fsts, x:snds)

-- |A slight specialization of 'withAll' that seems to be necessary
-- when the first argument to @c@ has RankNTypes
withAll' :: ((c -> c') -> (a -> b) -> b)
       -> [c -> c']
       -> ([a] -> b)
       -> b
withAll' = withAll

-- |Allows “with functions” to be nested, collecting all required
-- functions.
--
-- “with functions” refers to functions which, given a parameter of
-- type @c@, acquire a resource of type @a@, run a given function on
-- the resource and return the result of type @b@ after cleaning up
-- the resource.
withAll :: (c -> (a -> b) -> b) -- ^The “with function”
        -> [c]                  -- ^The arguments to the “with
                                -- function“
        -> ([a] -> b)           -- ^The function to run on the
                                -- resources
        -> b
withAll with args action = foldl (\acc arg resources -> with arg $
                                                        \resource -> acc $ resource : resources
                                 ) action args []
